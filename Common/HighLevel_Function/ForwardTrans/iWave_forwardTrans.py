import copy
import os
import time

import numpy as np
import torch
from PIL import Image
from torch.autograd import Variable
import torch.nn as nn
from torch.nn import functional as F

import Common.utils.iWave_utils.arithmetic_coding as ac
import Common.models.iWave_models.Model as Model
from Common.utils.iWave_utils.utils import (find_min_and_max, img2patch, img2patch_padding, yuv2rgb_lossless, patch2img,
                                            rgb2yuv, yuv2rgb, rgb2yuv_lossless, write_binary, to_variable, qp_shifts,
                                            model_lambdas, model_qps)

class iWave_ForwardTrans(nn.Module):
    def __init__(self, header,  **kwargs):
        super().__init__()
        self.header = header
        self.models_dict = None

    def encode(self, image, header):
        header.img_name = image
        self.header = header

        if header.isLossless == 0:
            self.enc_lossy(header)
        else:
            self.enc_lossless(header, header.recon_dir)

    def enc_lossless(self, header, recon_path):
        assert header.isLossless == 1

        if not os.path.exists(header.bin_dir):
            os.makedirs(header.bin_dir)
        if not os.path.exists(header.log_dir):
            os.makedirs(header.log_dir)
        if not os.path.exists(header.recon_dir):
            os.makedirs(header.recon_dir)

        logfile = open(header.log_dir + '/enc_log.txt', 'a')

        assert header.model_qp == 27
        assert header.qp_shift == 0
        init_scale = qp_shifts[header.model_qp][header.qp_shift]
        print(init_scale)
        logfile.write(str(init_scale) + '\n')
        logfile.flush()

        code_block_size = header.code_block_size

        bin_name = os.path.splitext(os.path.basename(header.img_name))[0] + '_' + str(header.model_qp) + '_' + str(header.qp_shift)
        bit_out = ac.CountingBitOutputStream(
            bit_out=ac.BitOutputStream(open(header.bin_dir + '/' + bin_name + '.bin', "wb")))
        enc = ac.ArithmeticEncoder(bit_out)
        freqs_resolution = 1e6

        freqs = ac.SimpleFrequencyTable(np.ones([2], dtype=np.int))



        trans_steps = 4

        img_path = header.img_name

        with torch.no_grad():
            start = time.time()

            print(img_path)
            logfile.write(img_path + '\n')
            logfile.flush()

            img = Image.open(img_path)
            img = np.array(img, dtype=np.float32)
            original_img = copy.deepcopy(img)

            img = rgb2yuv_lossless(img).astype(np.float32)
            img = torch.from_numpy(img)
            # img -> [n,c,h,w]
            img = img.unsqueeze(0)
            img = img.permute(0, 3, 1, 2)
            # original_img = img

            # img -> (%16 == 0)
            size = img.size()
            height = size[2]
            width = size[3]

            header.write_header_to_stream(enc, img)

            pad_h = int(np.ceil(height / 16)) * 16 - height
            pad_w = int(np.ceil(width / 16)) * 16 - width
            paddings = (0, pad_w, 0, pad_h)
            img = F.pad(img, paddings, 'replicate')

            # img -> [3,1,h,w], YUV in batch dim
            # img = rgb2yuv(img)
            img = img.permute(1, 0, 2, 3)

            input_img_v = to_variable(img)
            LL, HL_list, LH_list, HH_list = self.models_dict['transform'].forward_trans(input_img_v)

            min_v, max_v = find_min_and_max(LL, HL_list, LH_list, HH_list)
            # for all models, the quantized coefficients are in the range of [-6016, 12032]
            # 15 bits to encode this range
            for i in range(3):
                for j in range(13):
                    tmp = min_v[i, j] + 6016
                    write_binary(enc, tmp, 15)
                    tmp = max_v[i, j] + 6016
                    write_binary(enc, tmp, 15)
            yuv_low_bound = min_v.min(axis=0)
            yuv_high_bound = max_v.max(axis=0)
            shift_min = min_v - yuv_low_bound
            shift_max = max_v - yuv_low_bound

            subband_h = [(height + pad_h) // 2, (height + pad_h) // 4, (height + pad_h) // 8, (height + pad_h) // 16]
            subband_w = [(width + pad_w) // 2, (width + pad_w) // 4, (width + pad_w) // 8, (width + pad_w) // 16]

            padding_sub_h = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_h]
            padding_sub_w = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_w]

            coded_coe_num = 0

            # compress LL
            tmp_stride = subband_w[3] + padding_sub_w[3]
            tmp_hor_num = tmp_stride // code_block_size
            paddings = (0, padding_sub_w[3], 0, padding_sub_h[3])
            enc_LL = F.pad(LL, paddings, "constant")
            enc_LL = img2patch(enc_LL, code_block_size, code_block_size, code_block_size)
            paddings = (6, 6, 6, 6)
            enc_LL = F.pad(enc_LL, paddings, "constant")
            for h_i in range(code_block_size):
                for w_i in range(code_block_size):
                    cur_ct = copy.deepcopy(enc_LL[:, :, h_i:h_i + 13, w_i:w_i + 13])
                    cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                    cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                    prob = self.models_dict['entropy_LL'](cur_ct, yuv_low_bound[0], yuv_high_bound[0])

                    prob = prob.cpu().data.numpy()
                    index = enc_LL[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                    # index = index - lower_bound

                    prob = prob * freqs_resolution
                    prob = prob.astype(np.int64)
                    for sample_idx, prob_sample in enumerate(prob):
                        coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                 h_i * tmp_stride + \
                                 ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                 w_i
                        if (coe_id % tmp_stride) < subband_w[3] and (coe_id // tmp_stride) < subband_h[3]:
                            yuv_flag = sample_idx % 3
                            # if True:
                            if shift_min[yuv_flag, 0] < shift_max[yuv_flag, 0]:
                                freqs = ac.SimpleFrequencyTable(
                                    prob_sample[shift_min[yuv_flag, 0]:shift_max[yuv_flag, 0] + 1])
                                data = index[sample_idx] - min_v[yuv_flag, 0]
                                assert data >= 0
                                enc.write(freqs, data)
                            coded_coe_num = coded_coe_num + 1
            print('LL encoded...')

            # LL = LL * used_scale

            for i in range(trans_steps):
                j = trans_steps - 1 - i
                tmp_stride = subband_w[j] + padding_sub_w[j]
                tmp_hor_num = tmp_stride // code_block_size
                # compress HL
                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                enc_oth = F.pad(HL_list[j], paddings, "constant")
                enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                paddings = (6, 6, 6, 6)
                enc_oth = F.pad(enc_oth, paddings, "constant")

                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                context = F.pad(LL, paddings, "reflect")
                paddings = (6, 6, 6, 6)
                context = F.pad(context, paddings, "reflect")
                context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                for h_i in range(code_block_size):
                    for w_i in range(code_block_size):
                        cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                        cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                        cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                        cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                        prob = self.models_dict['entropy_HL'](cur_ct, cur_context,
                                                         yuv_low_bound[3 * j + 1], yuv_high_bound[3 * j + 1], j)

                        prob = prob.cpu().data.numpy()
                        index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                        # index = index - lower_bound

                        prob = prob * freqs_resolution
                        prob = prob.astype(np.int64)
                        for sample_idx, prob_sample in enumerate(prob):
                            coe_id = ((
                                                  sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                     h_i * tmp_stride + \
                                     ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                     w_i
                            if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                yuv_flag = sample_idx % 3
                                # if True:
                                if shift_min[yuv_flag, 3 * j + 1] < shift_max[yuv_flag, 3 * j + 1]:
                                    freqs = ac.SimpleFrequencyTable(
                                        prob_sample[shift_min[yuv_flag, 3 * j + 1]:shift_max[yuv_flag, 3 * j + 1] + 1])
                                    data = index[sample_idx] - min_v[yuv_flag, 3 * j + 1]
                                    assert data >= 0
                                    enc.write(freqs, data)
                                coded_coe_num = coded_coe_num + 1

                print('HL' + str(j) + ' encoded...')

                # HL_list[j] = HL_list[j]*used_scale

                # compress LH
                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                enc_oth = F.pad(LH_list[j], paddings, "constant")
                enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                paddings = (6, 6, 6, 6)
                enc_oth = F.pad(enc_oth, paddings, "constant")

                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                context = F.pad(torch.cat((LL, HL_list[j]), dim=1), paddings, "reflect")
                paddings = (6, 6, 6, 6)
                context = F.pad(context, paddings, "reflect")
                context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                for h_i in range(code_block_size):
                    for w_i in range(code_block_size):
                        cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                        cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                        cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                        cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                        prob = self.models_dict['entropy_LH'](cur_ct, cur_context,
                                                         yuv_low_bound[3 * j + 2], yuv_high_bound[3 * j + 2], j)

                        prob = prob.cpu().data.numpy()
                        index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                        # index = index - lower_bound

                        prob = prob * freqs_resolution
                        prob = prob.astype(np.int64)
                        for sample_idx, prob_sample in enumerate(prob):
                            coe_id = ((
                                                  sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                     h_i * tmp_stride + \
                                     ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                     w_i
                            if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                yuv_flag = sample_idx % 3
                                # if True:
                                if shift_min[yuv_flag, 3 * j + 2] < shift_max[yuv_flag, 3 * j + 2]:
                                    freqs = ac.SimpleFrequencyTable(
                                        prob_sample[shift_min[yuv_flag, 3 * j + 2]:shift_max[yuv_flag, 3 * j + 2] + 1])
                                    data = index[sample_idx] - min_v[yuv_flag, 3 * j + 2]
                                    assert data >= 0
                                    enc.write(freqs, data)
                                coded_coe_num = coded_coe_num + 1
                print('LH' + str(j) + ' encoded...')

                # LH_list[j] = LH_list[j] * used_scale

                # compress HH
                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                enc_oth = F.pad(HH_list[j], paddings, "constant")
                enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                paddings = (6, 6, 6, 6)
                enc_oth = F.pad(enc_oth, paddings, "constant")

                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                context = F.pad(torch.cat((LL, HL_list[j], LH_list[j]), dim=1), paddings, "reflect")
                paddings = (6, 6, 6, 6)
                context = F.pad(context, paddings, "reflect")
                context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                for h_i in range(code_block_size):
                    for w_i in range(code_block_size):
                        cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                        cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                        cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                        cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                        prob = self.models_dict['entropy_HH'](cur_ct, cur_context,
                                                         yuv_low_bound[3 * j + 3], yuv_high_bound[3 * j + 3], j)

                        prob = prob.cpu().data.numpy()
                        index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                        # index = index - lower_bound

                        prob = prob * freqs_resolution
                        prob = prob.astype(np.int64)
                        for sample_idx, prob_sample in enumerate(prob):
                            coe_id = ((
                                                  sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                     h_i * tmp_stride + \
                                     ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                     w_i
                            if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                yuv_flag = sample_idx % 3
                                # if True:
                                if shift_min[yuv_flag, 3 * j + 3] < shift_max[yuv_flag, 3 * j + 3]:
                                    freqs = ac.SimpleFrequencyTable(
                                        prob_sample[shift_min[yuv_flag, 3 * j + 3]:shift_max[yuv_flag, 3 * j + 3] + 1])
                                    data = index[sample_idx] - min_v[yuv_flag, 3 * j + 3]
                                    assert data >= 0
                                    enc.write(freqs, data)
                                coded_coe_num = coded_coe_num + 1

                print('HH' + str(j) + ' encoded...')
                # HH_list[j] = HH_list[j] * used_scale

                LL = self.models_dict['transform'].inverse_trans(LL, HL_list[j], LH_list[j], HH_list[j], j)

            assert (coded_coe_num == (height + pad_h) * (width + pad_w) * 3)

            recon = LL.permute(1, 0, 2, 3)
            recon = recon[:, :, 0:height, 0:width]
            recon = recon[0, :, :, :]
            recon = recon.permute(1, 2, 0)
            recon = recon.cpu().data.numpy()
            recon = yuv2rgb_lossless(recon).astype(np.float32)

            mse = np.mean((recon - original_img) ** 2)
            psnr = (10. * np.log10(255. * 255. / mse))

            recon = np.clip(recon, 0., 255.).astype(np.uint8)
            img = Image.fromarray(recon, 'RGB')
            img.save(header.recon_dir + '/' + bin_name + '.png')

            enc.finish()
            print('encoding finished!')
            logfile.write('encoding finished!' + '\n')
            end = time.time()
            print('Encoding-time: ', end - start)
            logfile.write('Encoding-time: ' + str(end - start) + '\n')

            bit_out.close()
            print('bit_out closed!')
            logfile.write('bit_out closed!' + '\n')

            filesize = bit_out.num_bits / height / width
            print('BPP: ', filesize)
            logfile.write('BPP: ' + str(filesize) + '\n')
            logfile.flush()

            print('PSNR: ', psnr)
            logfile.write('PSNR: ' + str(psnr) + '\n')
            logfile.flush()

        logfile.close()

    def enc_lossy(self, header):

        assert header.isLossless == 0

        if not os.path.exists(header.bin_dir):
            os.makedirs(header.bin_dir)
        if not os.path.exists(header.log_dir):
            os.makedirs(header.log_dir)
        if not os.path.exists(header.recon_dir):
            os.makedirs(header.recon_dir)

        code_block_size = header.code_block_size

        bin_name = os.path.splitext(os.path.basename(header.img_name))[0] + '_' + str(header.model_qp) + '_' + str(header.qp_shift)
        bit_out = ac.CountingBitOutputStream(
            bit_out=ac.BitOutputStream(open(os.path.join(header.bin_dir, bin_name + '.bin'), "wb")))
        enc = ac.ArithmeticEncoder(bit_out)
        freqs_resolution = 1e6

        freqs = ac.SimpleFrequencyTable(np.ones([2], dtype=np.int))

        trans_steps = 4

        img_path = header.img_name

        logfile = open(os.path.join(header.log_dir, 'enc_log_{}.txt'.format(bin_name)), 'a')

        init_scale = qp_shifts[header.model_qp][header.qp_shift]
        print(init_scale)
        logfile.write(str(init_scale) + '\n')
        logfile.flush()

        with torch.no_grad():
            start = time.time()

            print(img_path)
            logfile.write(img_path + '\n')
            logfile.flush()

            img = Image.open(img_path)
            img = np.array(img, dtype=np.float32)
            img = torch.from_numpy(img)
            # img -> [n,c,h,w]
            img = img.unsqueeze(0)
            img = img.permute(0, 3, 1, 2)
            original_img = img

            # img -> (%16 == 0)
            size = img.size()
            height = size[2]
            width = size[3]
            # encode height and width, in the range of [0, 2^15=32768]
            header.write_header_to_stream(enc, img)

            pad_h = int(np.ceil(height / 16)) * 16 - height
            pad_w = int(np.ceil(width / 16)) * 16 - width
            paddings = (0, pad_w, 0, pad_h)
            img = F.pad(img, paddings, 'replicate')

            # img -> [3,1,h,w], YUV in batch dim
            img = rgb2yuv(img)
            img = img.permute(1, 0, 2, 3)

            input_img_v = to_variable(img)
            LL, HL_list, LH_list, HH_list, used_scale = self.models_dict['transform'].forward_trans(input_img_v)

            min_v, max_v = find_min_and_max(LL, HL_list, LH_list, HH_list)
            # for all models, the quantized coefficients are in the range of [-6016, 12032]
            # 15 bits to encode this range
            for i in range(3):
                for j in range(13):
                    tmp = min_v[i, j] + 6016
                    write_binary(enc, tmp, 15)
                    tmp = max_v[i, j] + 6016
                    write_binary(enc, tmp, 15)
            yuv_low_bound = min_v.min(axis=0)
            yuv_high_bound = max_v.max(axis=0)
            shift_min = min_v - yuv_low_bound
            shift_max = max_v - yuv_low_bound

            subband_h = [(height + pad_h) // 2, (height + pad_h) // 4, (height + pad_h) // 8, (height + pad_h) // 16]
            subband_w = [(width + pad_w) // 2, (width + pad_w) // 4, (width + pad_w) // 8, (width + pad_w) // 16]

            padding_sub_h = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_h]
            padding_sub_w = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_w]

            coded_coe_num = 0

            # compress LL
            tmp_stride = subband_w[3] + padding_sub_w[3]
            tmp_hor_num = tmp_stride // code_block_size
            paddings = (0, padding_sub_w[3], 0, padding_sub_h[3])
            enc_LL = F.pad(LL, paddings, "constant")
            enc_LL = img2patch(enc_LL, code_block_size, code_block_size, code_block_size)
            paddings = (6, 6, 6, 6)
            enc_LL = F.pad(enc_LL, paddings, "constant")
            for h_i in range(code_block_size):
                for w_i in range(code_block_size):
                    cur_ct = copy.deepcopy(enc_LL[:, :, h_i:h_i + 13, w_i:w_i + 13])
                    cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                    cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                    prob = self.models_dict['entropy_LL'](cur_ct, yuv_low_bound[0], yuv_high_bound[0])

                    prob = prob.cpu().data.numpy()
                    index = enc_LL[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                    # index = index - lower_bound

                    prob = prob * freqs_resolution
                    prob = prob.astype(np.int64)
                    for sample_idx, prob_sample in enumerate(prob):
                        coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                 h_i * tmp_stride + \
                                 ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                 w_i
                        if (coe_id % tmp_stride) < subband_w[3] and (coe_id // tmp_stride) < subband_h[3]:
                            yuv_flag = sample_idx % 3
                            # if True:
                            if shift_min[yuv_flag, 0] < shift_max[yuv_flag, 0]:
                                freqs = ac.SimpleFrequencyTable(
                                    prob_sample[shift_min[yuv_flag, 0]:shift_max[yuv_flag, 0] + 1])
                                data = index[sample_idx] - min_v[yuv_flag, 0]
                                assert data >= 0
                                enc.write(freqs, data)
                            coded_coe_num = coded_coe_num + 1
            print('LL encoded...')

            LL = LL * used_scale

            for i in range(trans_steps):
                j = trans_steps - 1 - i
                tmp_stride = subband_w[j] + padding_sub_w[j]
                tmp_hor_num = tmp_stride // code_block_size
                # compress HL
                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                enc_oth = F.pad(HL_list[j], paddings, "constant")
                enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                paddings = (6, 6, 6, 6)
                enc_oth = F.pad(enc_oth, paddings, "constant")

                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                context = F.pad(LL, paddings, "reflect")
                paddings = (6, 6, 6, 6)
                context = F.pad(context, paddings, "reflect")
                context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                for h_i in range(code_block_size):
                    for w_i in range(code_block_size):
                        cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                        cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                        cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                        cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                        prob = self.models_dict['entropy_HL'](cur_ct, cur_context,
                                                         yuv_low_bound[3 * j + 1], yuv_high_bound[3 * j + 1], j)

                        prob = prob.cpu().data.numpy()
                        index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                        # index = index - lower_bound

                        prob = prob * freqs_resolution
                        prob = prob.astype(np.int64)
                        for sample_idx, prob_sample in enumerate(prob):
                            coe_id = ((
                                                  sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                     h_i * tmp_stride + \
                                     ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                     w_i
                            if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                yuv_flag = sample_idx % 3
                                # if True:
                                if shift_min[yuv_flag, 3 * j + 1] < shift_max[yuv_flag, 3 * j + 1]:
                                    freqs = ac.SimpleFrequencyTable(
                                        prob_sample[shift_min[yuv_flag, 3 * j + 1]:shift_max[yuv_flag, 3 * j + 1] + 1])
                                    data = index[sample_idx] - min_v[yuv_flag, 3 * j + 1]
                                    assert data >= 0
                                    enc.write(freqs, data)
                                coded_coe_num = coded_coe_num + 1

                print('HL' + str(j) + ' encoded...')

                HL_list[j] = HL_list[j] * used_scale

                # compress LH
                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                enc_oth = F.pad(LH_list[j], paddings, "constant")
                enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                paddings = (6, 6, 6, 6)
                enc_oth = F.pad(enc_oth, paddings, "constant")

                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                context = F.pad(torch.cat((LL, HL_list[j]), dim=1), paddings, "reflect")
                paddings = (6, 6, 6, 6)
                context = F.pad(context, paddings, "reflect")
                context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                for h_i in range(code_block_size):
                    for w_i in range(code_block_size):
                        cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                        cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                        cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                        cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                        prob = self.models_dict['entropy_LH'](cur_ct, cur_context,
                                                         yuv_low_bound[3 * j + 2], yuv_high_bound[3 * j + 2], j)

                        prob = prob.cpu().data.numpy()
                        index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                        # index = index - lower_bound

                        prob = prob * freqs_resolution
                        prob = prob.astype(np.int64)
                        for sample_idx, prob_sample in enumerate(prob):
                            coe_id = ((
                                                  sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                     h_i * tmp_stride + \
                                     ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                     w_i
                            if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                yuv_flag = sample_idx % 3
                                # if True:
                                if shift_min[yuv_flag, 3 * j + 2] < shift_max[yuv_flag, 3 * j + 2]:
                                    freqs = ac.SimpleFrequencyTable(
                                        prob_sample[shift_min[yuv_flag, 3 * j + 2]:shift_max[yuv_flag, 3 * j + 2] + 1])
                                    data = index[sample_idx] - min_v[yuv_flag, 3 * j + 2]
                                    assert data >= 0
                                    enc.write(freqs, data)
                                coded_coe_num = coded_coe_num + 1
                print('LH' + str(j) + ' encoded...')

                LH_list[j] = LH_list[j] * used_scale

                # compress HH
                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                enc_oth = F.pad(HH_list[j], paddings, "constant")
                enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                paddings = (6, 6, 6, 6)
                enc_oth = F.pad(enc_oth, paddings, "constant")

                paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                context = F.pad(torch.cat((LL, HL_list[j], LH_list[j]), dim=1), paddings, "reflect")
                paddings = (6, 6, 6, 6)
                context = F.pad(context, paddings, "reflect")
                context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                for h_i in range(code_block_size):
                    for w_i in range(code_block_size):
                        cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                        cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                        cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                        cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                        prob = self.models_dict['entropy_HH'](cur_ct, cur_context,
                                                         yuv_low_bound[3 * j + 3], yuv_high_bound[3 * j + 3], j)

                        prob = prob.cpu().data.numpy()
                        index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                        # index = index - lower_bound

                        prob = prob * freqs_resolution
                        prob = prob.astype(np.int64)
                        for sample_idx, prob_sample in enumerate(prob):
                            coe_id = ((
                                                  sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                     h_i * tmp_stride + \
                                     ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                     w_i
                            if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                yuv_flag = sample_idx % 3
                                # if True:
                                if shift_min[yuv_flag, 3 * j + 3] < shift_max[yuv_flag, 3 * j + 3]:
                                    freqs = ac.SimpleFrequencyTable(
                                        prob_sample[shift_min[yuv_flag, 3 * j + 3]:shift_max[yuv_flag, 3 * j + 3] + 1])
                                    data = index[sample_idx] - min_v[yuv_flag, 3 * j + 3]
                                    assert data >= 0
                                    enc.write(freqs, data)
                                coded_coe_num = coded_coe_num + 1

                print('HH' + str(j) + ' encoded...')
                HH_list[j] = HH_list[j] * used_scale

                LL = self.models_dict['transform'].inverse_trans(LL, HL_list[j], LH_list[j], HH_list[j], j)

            assert (coded_coe_num == (height + pad_h) * (width + pad_w) * 3)

            recon = LL
            recon = yuv2rgb(recon.permute(1, 0, 2, 3))
            recon = recon[:, :, 0:height, 0:width]

            if header.second_step_filtering:

                recon = self.models_dict['post'](recon)

                if height * width > 1080 * 1920:
                    h_list = [0, height // 2, height]
                    w_list = [0, width // 2, width]
                    k_ = 2
                else:
                    h_list = [0, height]
                    w_list = [0, width]
                    k_ = 1
                gan_rgb_post = torch.zeros_like(recon)
                for _i in range(k_):
                    for _j in range(k_):
                        pad_start_h = max(h_list[_i] - 64, 0) - h_list[_i]
                        pad_end_h = min(h_list[_i + 1] + 64, height) - h_list[_i + 1]
                        pad_start_w = max(w_list[_j] - 64, 0) - w_list[_j]
                        pad_end_w = min(w_list[_j + 1] + 64, width) - w_list[_j + 1]
                        tmp = self.models_dict['postGAN'](recon[:, :, h_list[_i] + pad_start_h:h_list[_i + 1] + pad_end_h,
                                                     w_list[_j] + pad_start_w:w_list[_j + 1] + pad_end_w])
                        gan_rgb_post[:, :, h_list[_i]:h_list[_i + 1], w_list[_j]:w_list[_j + 1]] = tmp[:, :,
                                                                                                   -pad_start_h:
                                                                                                   tmp.size()[
                                                                                                       2] - pad_end_h,
                                                                                                   -pad_start_w:
                                                                                                   tmp.size()[
                                                                                                       3] - pad_end_w]
                recon = gan_rgb_post
            else:

                h_list = [0, height // 3, height // 3 * 2, height]
                w_list = [0, width // 3, width // 3 * 2, width]
                k_ = 3
                rgb_post = torch.zeros_like(recon)
                for _i in range(k_):
                    for _j in range(k_):
                        pad_start_h = max(h_list[_i] - 64, 0) - h_list[_i]
                        pad_end_h = min(h_list[_i + 1] + 64, height) - h_list[_i + 1]
                        pad_start_w = max(w_list[_j] - 64, 0) - w_list[_j]
                        pad_end_w = min(w_list[_j + 1] + 64, width) - w_list[_j + 1]
                        tmp = self.models_dict['post'](recon[:, :, h_list[_i] + pad_start_h:h_list[_i + 1] + pad_end_h,
                                                  w_list[_j] + pad_start_w:w_list[_j + 1] + pad_end_w])
                        rgb_post[:, :, h_list[_i]:h_list[_i + 1], w_list[_j]:w_list[_j + 1]] = tmp[:, :,
                                                                                               -pad_start_h:tmp.size()[
                                                                                                                2] - pad_end_h,
                                                                                               -pad_start_w:tmp.size()[
                                                                                                                3] - pad_end_w]
                recon = rgb_post

            recon = torch.clamp(torch.round(recon), 0., 255.)
            mse = torch.mean((recon.cpu() - original_img) ** 2)
            psnr = (10. * torch.log10(255. * 255. / mse)).item()

            recon = recon[0, :, :, :]
            recon = recon.permute(1, 2, 0)
            recon = recon.cpu().data.numpy().astype(np.uint8)
            img = Image.fromarray(recon, 'RGB')
            img.save(os.path.join(header.recon_dir, bin_name + '.png'))

            enc.finish()
            print('encoding finished!')
            logfile.write('encoding finished!' + '\n')
            end = time.time()
            print('Encoding-time: ', end - start)
            logfile.write('Encoding-time: ' + str(end - start) + '\n')

            bit_out.close()
            print('bit_out closed!')
            logfile.write('bit_out closed!' + '\n')

            filesize = bit_out.num_bits / height / width
            print('BPP: ', filesize)
            logfile.write('BPP: ' + str(filesize) + '\n')
            logfile.flush()

            print('PSNR: ', psnr)
            logfile.write('PSNR: ' + str(psnr) + '\n')
            logfile.flush()

        logfile.close()
