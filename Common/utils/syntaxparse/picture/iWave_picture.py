import struct
import json

import Common.utils.iWave_utils.arithmetic_coding as ac
from Common.utils.iWave_utils.utils import (find_min_and_max, img2patch, img2patch_padding,
                   model_lambdas, patch2img, qp_shifts, rgb2yuv, yuv2rgb, dec_binary, write_binary, model_qps)


class iWave_picture():
    def __init__(self):
        self.picture_header = iWave_picture_header()

    def write_header_to_stream():
        raise NotImplemented

    def read_header_from_stream(self, f):
        self.picture_header.read_header_from_stream(f)

    def update(self, picture_syntax, input_imgae):
        self.picture_header.update(picture_syntax, input_imgae)


class iWave_picture_header():
    def __init__(self):
        self.dec = None

        self.profile_id = 0
        self.level_id = 0
        self.isLossless = 0
        self.picture_format = 0
        self.sample_precision = 0
        self.vertical_size = 0
        self.horizontal_size = 0
        self.log2_picture_height_minus6 = 0
        self.log2_picture_width_minus6 = 0
        self.log2_block_height_minus3 = 6
        self.log2_block_width_minus3 = 6
        self.log2_subband_block_height_minus2 = 0
        self.log2_subband_block_width_minus2 = 0
        self.encoding_precision = 0
        self.first_step_filtering = 1
        self.second_step_filtering = 0

        self.resolution_level = 4
        self.model_index = 27
        self.block_qp_shift = 0
        self.no_estimated_delta_scale = 0

        self.model_qps = model_qps
        self.model_lambdas = model_lambdas
        self.qp_shifts = qp_shifts

        self.code_block_size = 64  # the value should be equal to 2 ** log2_block_height_minus3
        self.model_path = None

        self.img_name = None

    def write_header_to_stream(self, enc, image):
        size = image.size()
        self.vertical_size = size[2]
        self.horizontal_size = size[3]

        write_binary(enc, 1, 2)
        write_binary(enc, 1, 2)
        write_binary(enc, 1, 2)
        write_binary(enc, 0, 2)

        write_binary(enc, self.profile_id, 8)
        write_binary(enc, self.level_id, 8)
        write_binary(enc, self.isLossless, 1)
        write_binary(enc, self.picture_format, 3)
        write_binary(enc, self.sample_precision, 3)

        write_binary(enc, self.vertical_size, 14)
        write_binary(enc, self.horizontal_size, 14)

        write_binary(enc, self.log2_picture_height_minus6, 4)
        write_binary(enc, self.log2_picture_width_minus6, 4)
        write_binary(enc, self.log2_block_height_minus3, 4)
        write_binary(enc, self.log2_block_width_minus3, 4)
        write_binary(enc, self.log2_subband_block_height_minus2, 3)
        write_binary(enc, self.log2_subband_block_width_minus2, 3)
        write_binary(enc, self.encoding_precision, 3)

        if self.isLossless == 0:
            write_binary(enc, self.first_step_filtering, 1)
            write_binary(enc, self.second_step_filtering, 1)

        write_binary(enc, self.resolution_level, 3)
        write_binary(enc, self.model_index, 7)

        if self.isLossless == 0:
            write_binary(enc, self.block_qp_shift, 3)
            write_binary(enc, self.no_estimated_delta_scale, 1)

        num_held_bits = 94
        if self.isLossless:
            num_held_bits = num_held_bits + 6
        num_held_bits = num_held_bits % 8

        write_binary(enc, 0, num_held_bits)

        return enc

    def read_header_from_stream(self, file_object):

        bit_in = ac.BitInputStream(file_object)
        dec = ac.ArithmeticDecoder(bit_in)

        self.profile_id = dec_binary(dec, 8)
        self.level_id = dec_binary(dec, 8)
        self.isLossless = dec_binary(dec, 1)
        self.picture_format = dec_binary(dec, 3)
        self.sample_precision = dec_binary(dec, 3)
        self.vertical_size = dec_binary(dec, 14)
        self.horizontal_size = dec_binary(dec, 14)
        self.log2_picture_height_minus6 = dec_binary(dec, 4)
        self.log2_picture_width_minus6 = dec_binary(dec, 4)
        self.log2_block_height_minus3 = dec_binary(dec, 4)
        self.log2_block_width_minus3 = dec_binary(dec, 4)
        self.log2_subband_block_height_minus2 = dec_binary(dec, 3)
        self.log2_subband_block_width_minus2 = dec_binary(dec, 3)
        self.encoding_precision = dec_binary(dec, 3)
        if self.isLossless == 0:
            self.first_step_filtering = dec_binary(dec, 1)
            self.second_step_filtering = dec_binary(dec, 1)

        self.resolution_level = dec_binary(dec, 3)
        self.model_index = dec_binary(dec, 7)

        if self.isLossless == 0:
            self.block_qp_shift = dec_binary(dec, 3)
            self.no_estimated_delta_scale = dec_binary(dec, 1)

        num_held_bits = 94
        if self.isLossless:
            num_held_bits = num_held_bits + 6
        num_held_bits = num_held_bits % 8

        dec_binary(dec, num_held_bits)

        self.dec = dec
        return dec

    def update(self, picture_syntax, input_imgae):

        self.isLossless = picture_syntax["isLossless"]
        self.code_block_size = picture_syntax["code_block_size"]
        self.model_index = picture_syntax["model_index"]
        self.qp_shift = picture_syntax["qp_shift"]

        self.model_qp = picture_syntax["model_index"]
        self.model_path = picture_syntax["model_path"]
        self.recon_dir = picture_syntax["recon_dir"]
        self.bin_dir = picture_syntax["bin_dir"]
        self.log_dir = picture_syntax["log_dir"]

        self.block_qp_shift = self.qp_shift

        if self.model_qp > 16.5:         # Perceptual models have PostGAN
            self.first_step_filtering = 1
            self.second_step_filtering = 1

        return