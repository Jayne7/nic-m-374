# Reference software for IEEE 1857.11 Standard for Neural Network-Based Image Coding

The software is the reference software for IEEE 1857.11 standard for neural network-based image coding, including encoder, decoder and training functionalities.

Reference software is useful in aiding users of a video coding standard to establish and test conformance and interoperability, and to educate users and demonstrate the capabilities of the standard. For these purposes, this software is provided as an aid for the study and implementation of Neural Network-Based Image Coding.

The software has been developed by the IEEE 1857.11 Working Group.

## Table of Contents

- [Reference software for IEEE 1857.11 Standard for Neural Network-Based Image Coding](#reference-software-for-ieee-185711-standard-for-neural-network-based-image-coding)
  - [Table of Contents](#table-of-contents)
    - [Environment](#environment)
    - [BEE Usage](#bee-usage)
      - [Encoding](#encoding)
      - [Decoding](#decoding)
      - [Training](#training)
    - [iWave Usage](#iwave-usage)
      - [Encoding](#encoding-1)
      - [Decoding](#decoding-1)
      - [Training](#training-1)
    - [NIC Usage](#nic-usage)
      - [Encoding](#encoding-2)
      - [Decoding](#decoding-2)
      - [Training](#training-2)
    - [Possible Issues](#possible-issues)
    - [License](#license)
    - [Contributors](#contributors)


### Environment

Please read [requirements](requirements.txt) for details on environment of this software.

### BEE Usage

#### Encoding

* Encode a single picture-->
```
python3 Encoder/encode.py --input YourTestPic.png --output OutputBin.bin --device cuda --ckptdir PretrainedModelsFolder --target_rate 0.75 --cfg Encoder/cfg.json
```

* Encode all images under a folder-->
```
python3 Encoder/encode.py -inputPath YourTestPicFolder -outputPath OutputBinsFolder --device cuda --ckpt PretrainedModelsFolder --target_rate 0.75 --cfg Encoder/cfg.json
```

* Arguments Help
```
--input: Input image file path.
--output: Output bin file name.
--inputPath: Input images folder path.
--outputPath: Output bins folder path.
--device: CPU or GPU device. You can only use "cpu" or "cuda", defalut is "cuda".
--ckptdir: Checkpoint folder containing multiple pretrained models.
--target_rate: Target bpp. Default is a multiple rate list: [0.75,0.50,0.25,0.12,0.06]
--cfg: Path to the CfG file. This file is placed in the "Encoder" folder by default. We introduce the composition of this file in "Cfg Help".
```

* Cfg Help
```
The Cfg file of encoding mainly consists of two parts: "syntax of coding model selection" and "syntax of picture". You can modify relevant parameters as required.

Example:

"coding_model_seclection_syntax": {
        "bin2symbolIdx": 0,
        "arithmeticEngineIdx": 0,
        "synthesisTransformIdx": 0
    }

"0" denotes BEE task, "1" denotes iWave task, "2" denotes NIC task.

"picture_syntax":{

}

```
#### Decoding

* Decode a single bin-->
```
python3 Decoder/decode.py --input YourTestBin.bin --output OutputRecon.bin --device cuda --ckptdir PretrainedModelsFolder
```

* Decode all bins under a folder-->
```
python3 Decoder/decode.py --binpath YourTestBinsFolder --recpath OutputReconsFolder --device cuda --ckptdir PretrainedModelsFolder
```

* Arguments Help
```
--input: Input bin file path.
--output: Output reconstruction image file name.
--binpath: Input bins folder path.
--recpath: Output reconstruction images folder path.
--device: CPU or GPU device. You can only use "cpu" or "cuda", defalut is "cuda".
--ckptdir: Checkpoint folder containing multiple pretrained models.
```

#### Training

Please enter [Train](https://gitlab.com/NIC_software/NIC/-/tree/Encoder_IEEE1857software/Train) folder for details on our different tasks of training.

### iWave Usage

#### Encoding

* Encode a PNG file simply call:
```
python3 Encoder/encode.py --input YourTestPic.png --output OutputBin.bin --device cuda --ckptdir PretrainedModelsFolder --cfg Encoder/cfg/iwave_cfg/encode_iWave_lossy.cfg
```

* Cfg Help

The Cfg file of encoding mainly consists of two parts: "syntax of coding model selection" and "syntax of picture". You can modify relevant parameters as required.

Example:
```
{
	"coding_model_seclection_syntax": {
		"bin2symbolIdx": 1,
		"arithmeticEngineIdx": 1,
		"synthesisTransformIdx": 1
	},
	"picture_syntax": {
		"bin_dir": "D:/iwave/iwave_normal/out/",
		"log_dir": "D:/iwave/iwave_normal/out/",
		"recon_dir": "D:/iwave/iwave_normal/out/",
		"model_path": "D:/iwave/1857Models",
		"code_block_size": 64,
		"isLossless": 0,
		"model_index": 17,
		"qp_shift": 1,
		"isPostGAN": 1
	}
}

--bin_dir bitstream path. 
--log_dir output log path. 
--recon_dir output reconstructed images path. 
--model_path model path.
--isLossless A value of '1' indicates that lossless coding is used; a value of '0' indicates that lossy coding is used. The model_index should be set to 27 when you set the value to '1'.
--code_block_size It indicates the width of wavelet transform blocks. It's highly recommended to set the value to ‘64’.
--model_index It specifies the index of the model to be used by the decoding process. The range of model_index should be set from 0 to 12 if you want to get the results optimized for MSE; from 13 to 26 if you want to get the results optimized for perception. In lossy mode, if the model_index is no more than 17, the value of isPostGAN should be set as 0, and vice versa. The value of model_index should be set as 27 if you want to get the lossless results.
```

#### Decoding
* Decode a bitstream file simply call:
```
python3 Decoder/decode.py --input YourTestBin.bin --output OutputRecon.bin --device cuda --ckptdir PretrainedModelsFolder
```

#### Training

Please enter [Train](https://gitlab.com/NIC_software/NIC/-/tree/Encoder_IEEE1857software/Train) folder for details on our different tasks of training.

### Possible Issues

* AE integration issues -->

     In order to call the AE modules in the python file, we provide "setup.py" to generate link files for different platforms. The specific use methods are as follows:
```
linux,windows-> use command: python Common/models/BEE_models/setup.py build_ext --inplace   

```

### NIC Usage

#### Encoding

* Encode a PNG file simply call:
```
python3 Encoder/encode.py --input YourTestPic.png --output OutputBin.bin --device cuda --ckptdir PretrainedModelsFolder -m 0 --cfg Encoder/cfg/nic_default.cfg
```

* Arguments Help
```
--input: Input image file path.
--output: Output bin file name.
--inputPath: Input images folder path.
--outputPath: Output bins folder path.
--device: CPU or GPU device. You can only use "cpu" or "cuda", defalut is "cuda".
--ckptdir: Checkpoint folder containing multiple pretrained models.
--cfg: Path to the CfG file. This file is placed in the "Encoder" folder by default. We introduce the composition of this file in "Cfg Help".

--model: Model Index. [0-5] if variable-rate models are used. [0-15] if fixed-rate models are used.
--lamdbda_rd: Input lambda for variable-rate models
```

* Cfg Help

The Cfg file of encoding mainly consists of two parts: "syntax of coding model selection" and "syntax of picture". You can modify relevant parameters as required. More configurations can be modified in 'Common/utils/nic_utils/config.py'.

Example for NIC:
```
{
	"coding_model_seclection_syntax": {
		"bin2symbolIdx": 2,
		"arithmeticEngineIdx": 1,
		"synthesisTransformIdx": 2
	},
	"picture_syntax": {
    "nic": 0
	}
}
```

#### Decoding
* Dncode a bitstream file simply call:
```
python3 Decoder/decoder.py --input YourTestBin.bin --output OutputRecon.png --device cuda --ckptdir PretrainedModelsFolder
```

#### Training

Please enter [Train](https://gitlab.com/NIC_software/NIC/-/tree/Encoder_IEEE1857software/Train) folder for details on our different tasks of training.

### Possible Issues

* AE integration issues -->

     NIC uses a different Arithmetic coding module. In order to call the AE modules in the python file, NIC also provides "setup.py" to generate link files for different platforms. The specific use methods are as described [here](https://gitlab.com/NIC_software/NIC/-/blob/Encoder_IEEE1857software/Common/utils/nic_utils/AE/README.md).

### License

This project is licensed under the BSD-3 License - see the [LICENSE](LICENSE) file for details

### Contributors

Contact information of Contributors 


